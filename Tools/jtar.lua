package.path = package.path .. ";/usr/modules/?.lua"
local tarlib = require("jakobdev.tarlib")
local tArgs = {...}

if tArgs[1] == "list" then
    local sPath = shell.resolve(tArgs[2])
    local f = tarlib.open(sPath,"r")
    local tPrint = {{"Name","Type","Size"}}
    for _,i in ipairs(f.list()) do
        table.insert(tPrint,{i,f.type(i),tostring(f.size(i))})
    end
    textutils.pagedTabulate(table.unpack(tPrint))
elseif tArgs[1] == "extract" then
    local sPath = shell.resolve(tArgs[2])
    local f = tarlib.open(sPath,"r")
    f.extract(tArgs[3],shell.resolve(tArgs[4]))
elseif tArgs[1] == "extractall" then
    local sPath = shell.resolve(tArgs[2])
    local f = tarlib.open(sPath,"r")
    f.extractAll(shell.resolve(tArgs[3]))
elseif tArgs[1] == "create" then
    local sPath = shell.resolve(tArgs[2])
    local f = tarlib.open(sPath,"w")
    for n,i in ipairs(tArgs) do
        if n > 2 then
            sFullPath = shell.resolve(i)
            if fs.isDir(sFullPath) then
                f.addDirectory(sFullPath)
            else
                f.addFile(sFullPath)
            end
        end
    end
    f.close()
elseif tArgs[1] == "append" then
    local sPath = shell.resolve(tArgs[2])
    local f = tarlib.open(sPath,"a")
    for n,i in ipairs(tArgs) do
        if n > 2 then
            sFullPath = shell.resolve(i)
            if fs.isDir(sFullPath) then
                f.addDirectory(sFullPath)
            else
                f.addFile(sFullPath)
            end
        end
    end
    f.close()
elseif tArgs[1] == "version" then
    print("jtar Version 1.1 using tarlib Version " .. tarlib.version())
else
    print("Usages:")
    print("jtar list <tarfile>")
    print("jtar extract <tarfile> <filename> <destination>")
    print("jtar extractall <tarfile> <destination>")
    print("jtar create <tarfile> <paths>")
    print("jtar append <tarfile> <paths>")
    print("jtar version")
end
